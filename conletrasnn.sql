﻿DROP DATABASE IF EXISTS conletras;
CREATE DATABASE IF NOT EXISTS conletras;
USE conletras1;
CREATE TABLE a(
  a1 int AUTO_INCREMENT,
  a2 varchar (20),
  PRIMARY KEY (a1)
);
CREATE TABLE r (
  r1 varchar (30),
  rol1 varchar (30),
  rol2 varchar (30),
  PRIMARY KEY (rol1,rol2),
  CONSTRAINT ra FOREIGN KEY (rol1) REFERENCES a (a1),
  CONSTRAINT ra FOREIGN KEY (rol2) REFERENCES a (a1)
);
